<?php

return [
    'plugin' => [
        'name'        => 'Traducciones',
        'description' => 'Traducciones comunes para usos múltiples',
    ],
    'forms'  => [
        'fields' => [
            'name'           => 'Nombre',
            'email'          => 'Correo electronico',
            'phone'          => 'Telefono',
            'fax'            => 'Fax',
            'mobile'         => 'Celular',
            'url'            => 'Url',
            'icons'          => 'Iconos',
            'icon'           => 'Icono',
            'category'       => 'Categoría',
            'categories'     => 'Categorías',
            'feature'        => 'Atributo',
            'features'       => 'Atributos',
            'is_hidden'      => 'Oculto',
            'is_published'   => 'Publicado',
            'package'        => 'Paquete',
            'sort_order'     => 'Orden',
            'title'          => 'Título',
            'content'        => 'Contenido',
            'slug'           => 'Url amigable',
            'author_email'   => 'Correo del autor',
            'created'        => 'Creado',
            'created_date'   => 'Fecha de creación',
            'updated'        => 'Actualizado',
            'updated_date'   => 'Fecha de actualización',
            'published'      => 'Publicado',
            'published_date' => 'Fecha de publicación',
            'description'    => 'Descripcción',
            'details'        => 'Detalles',
        ]
    ],
];