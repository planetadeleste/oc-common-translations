<?php

return [
    'plugin' => [
        'name' => 'Translations',
        'description' => 'Common translations for multipurpose',
    ],
    'forms'  => [
        'fields' => [
            'name'           => 'Name',
            'email'          => 'Email',
            'phone'          => 'Phone',
            'fax'            => 'Fax',
            'mobile'         => 'Mobile',
            'url'            => 'Url',
            'icons'          => 'Icons',
            'icon'           => 'Icon',
            'category'       => 'Category',
            'categories'     => 'Categories',
            'feature'        => 'Feature',
            'features'       => 'Features',
            'is_hidden'      => 'Hidden',
            'is_published'   => 'Published',
            'package'        => 'Package',
            'sort_order'     => 'Sort Order',
            'title'          => 'Title',
            'content'        => 'Content',
            'slug'           => 'Slug',
            'author_email'   => 'Author Email',
            'created'        => 'Created',
            'created_date'   => 'Created date',
            'updated'        => 'Updated',
            'updated_date'   => 'Updated date',
            'published'      => 'Published',
            'published_date' => 'Published date',
            'description'    => 'Description',
            'details'        => 'Details',
        ]
    ],
];