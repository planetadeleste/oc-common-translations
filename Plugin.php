<?php namespace PlanetaDelEste\Tr;

use System\Classes\PluginBase;

/**
 * Tr Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'planetadeleste.tr::lang.plugin.name',
            'description' => 'planetadeleste.tr::lang.plugin.description',
            'author'      => 'PlanetaDelEste',
            'icon'        => 'icon-language'
        ];
    }

}
